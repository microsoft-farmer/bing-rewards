window.addEventListener("load", function () {
  setTimeout(function () {
    window.scrollTo(0, 0);
  }, 100);
});

function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}
var btn = document.getElementById("clickButton");
btn.addEventListener("click", async function () {
  const userInputTime = document.getElementById("userInputTime").value;
  const userInputNo = document.getElementById("userInputNo").value;
  const warn = document.getElementById("#warn");
  let flag = 0;
  localStorage.setItem("userInputTime", userInputTime);
  localStorage.setItem("userInputNo", userInputNo);
  let wt = parseInt(userInputTime) * 1000;
  if (userInputNo == "" || userInputTime == "") {
    warn.innerHTML = "Please enter number of searches";
    flag = 1;
  }
  if (isNaN(wt) || isNaN(userInputNo)) {
    warn.innerHTML = "Please enter numbers only";
    flag = 1;
  }if(wt<1000){
    warn.innerHTML = "Please select wait time atleast 1 sec";
    flag = 1;
  }
  if (flag == 1) {
    return;
  } else {
    try {
      for (let i = 1; i <= userInputNo; i++) {
        let j = "";
        let index = Math.floor(Math.random() * userInputNo);
        let k = index % i;
        if (index % 7 == 0) {
            j = "QSRE"+k;
        } else if(index % 5 == 0){
            if (k==0){
              j = "R5FD";
            }else{
              j = "R5FD"+k;
            }
        }else if (index % 3 == 0) {
            j = "QBLH"+k;
        } else if (index % 2 == 0) {
            j = "QBRE";
        } else {
            j = "QSRE";
        }
        let randf = await fetch(
          "https://random-word-api.vercel.app/api?words=2"
        );
        let rand = await randf.json();
        let r = Math.floor(Math.random() * 10);
        let openedWindow;
        if (r % 2 == 0) {
          openedWindow = window.open(
            `https://www.bing.com/search?q=${rand[0]}+${rand[1]}&FORM=${j}`,
            "_blank"
          );
        }else{
          openedWindow = window.open(
            `https://www.bing.com/search?q=${rand[0]}&FORM=${j}`,
            "_blank"
          );
        }
        if (index % 3 == 0) {
          await sleep(wt + 1000);
        }else {
          await sleep(wt);
        }
        if (openedWindow) {
          openedWindow.close();
        }
      }
    } catch (error) {
      console.error("Error:", error);
    }
  }
});
window.addEventListener("keypress", function(event) {
  console.log(event.key);
  if (event.key === "Enter") {
    event.preventDefault();
    btn.click();
  }
});